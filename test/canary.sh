#!/bin/bash

version_1=0
version_2=0
version_3=0

for ((i=1; i <= 100; i++)); do
  output=$(curl -s http://hello-world.info/canary)
  if [[ $output == "version 1.0.1" ]]; then
    version_1=$(expr $version_1 + 1)
  elif [[ $output == "version 1.0.2" ]]; then
    version_2=$(expr $version_2 + 1)
  elif [[ $output == "version 1.0.3" ]]; then
    version_3=$(expr $version_3 + 1)
  fi
done

echo "Version 1.0.1 count: $version_1"
echo "Version 1.0.2 count: $version_2"
echo "Version 1.0.3 count: $version_3"
